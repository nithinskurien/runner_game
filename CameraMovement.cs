﻿using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Transform player;
    private Transform planet;
    public Vector3 offset;

    // Update is called once per frame
    void Update()
    {   
        planet = GameObject.FindGameObjectWithTag("Planet").GetComponent<Transform>();
        Vector3 targetVec = (planet.position - player.position).normalized;
        transform.position = player.position + Quaternion.Euler(player.eulerAngles.x, player.eulerAngles.y, player.eulerAngles.z) * offset;
        transform.rotation = player.rotation * Quaternion.Euler(10, 0, 0);
    }
}
