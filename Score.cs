﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    public Transform player;
    public Text scoreText;
    public float distanceOffset;
    // Start is called before the first frame update
    void Start()
    {
        distanceOffset = player.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        scoreText.text = (player.transform.position.z - distanceOffset).ToString("0");
    }
}
