﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Rigidbody rb;
    public Material playerMaterial;
    private float forwardForce = 2000f;
    private float sideForce = 30f;
    private float jumpSideFactor = 0.75f;
    private float jumpForce = 700f;
    private float jumpGravity = 40f;
    private float glideForce = 20f;
    private float diveForce = 50f;
    public bool grounded = false;
    public bool jumped = false;
    public bool maxHeightBool = false;
    public float maxheight = 0f;
    public float distToGround = 0.0f;
    private float maxStamina = 100f;
    public float stamina = 100f;
    private float staminaDrain = 160f;
    private float staminaGain = 80f;

    // Update is called once per frame
    void FixedUpdate()
    {
        distToGround = GetComponent<Collider>().bounds.extents.y;
        IsGrounded();
        rb.AddForce(0, 0, forwardForce * Time.deltaTime);
        HorizontalMovement();
        VerticalMovement();
        MaximumHeightReached();
       // EndByBounds();
        UpdateStamina();
    }

    private void HorizontalMovement()
    {
        float horizontalForce;
        if (grounded)
        {
            horizontalForce = Input.GetAxis("Horizontal") * sideForce;      
        }
        else
        {
            horizontalForce = Input.GetAxis("Horizontal") * sideForce * jumpSideFactor;
        }
        rb.AddForce(horizontalForce * Time.deltaTime, 0, 0, ForceMode.VelocityChange);
        return;
    }


    private void VerticalMovement()
    {
        float verticalForce;
        float netForce = 0.0f;
        float staminaPercentageLeft;
        staminaPercentageLeft = stamina / maxStamina;
        verticalForce = Input.GetAxis("Vertical");
        if (verticalForce > 0 && grounded && stamina == 100f)
        {
            Debug.Log("Jump Initiated");
            // netForce += verticalForce * jumpForce;
            rb.AddForce(0, jumpForce * Time.deltaTime, 0, ForceMode.Impulse);
        }
        if (verticalForce > 0 && maxHeightBool && stamina > 0f)
        {
            Debug.Log("Max Velocity Reached");
            netForce += verticalForce * glideForce * staminaPercentageLeft;
        }
        if (verticalForce < 0 && !grounded)
        {
            Debug.Log("Diving");
            rb.AddForce(0, -diveForce * Time.deltaTime, 0, ForceMode.Impulse);
        }
        if (jumped && maxHeightBool)
        {
            Debug.Log("Gravity");
            netForce += -jumpGravity;
        }
        rb.AddForce(0, netForce * Time.deltaTime, 0, ForceMode.VelocityChange);
        return;
    }

    private void MaximumHeightReached()
    {
        float playerHeight = rb.transform.position.y;
        if (playerHeight > maxheight)
            maxheight = playerHeight;
        if (jumped && (playerHeight < maxheight))
        {
            maxHeightBool = true;
        }
    }

    private void IsGrounded()
    {
        grounded = Physics.Raycast(transform.position, -Vector3.up, distToGround + 0.1f);
        if (grounded)
        {
            jumped = false;
            maxHeightBool = false;
        }
        else
            jumped = true;
        return;
    }

    private void EndByBounds()
    {
         if (rb.position.y < -1f)
         {
            FindObjectOfType<GameManager>().EndGame();
         }
         return;
    }

    private void UpdateStamina()
    {
        if (grounded)
        {
            stamina = stamina + staminaGain * Time.deltaTime;
        }
        else
        {
            stamina = stamina - staminaDrain * Time.deltaTime;
        }
        if (stamina >= 100f)
        {
            stamina = 100f;
            playerMaterial.color = Color.green;
        }
        if (stamina <= 0f)
        {
            stamina = 0f;
            playerMaterial.color = Color.red;
        }
        return;
    }
}
