﻿using UnityEngine;

public class PlayerCollider : MonoBehaviour
{
    public PlayerMovement movement;
    public Material obstacleMaterial;
    // Update is called once per frame
    void OnCollisionEnter(Collision collisionInfo)
    {
        if (collisionInfo.collider.name == "Planet")
        {
            movement.grounded = true;
            movement.jumped = false;
            movement.maxHeightBool = false;
            movement.maxheight = 0.0f;
            obstacleMaterial.color = new Color(0.3f, 0.3f, 0.3f, 1f);
            movement.distToGround = collisionInfo.collider.bounds.extents.y;
        }
        if (collisionInfo.collider.tag == "Obstacles")
        {
            movement.enabled = false; 
            collisionInfo.gameObject.GetComponent<Renderer>().material.color = Color.red;
            FindObjectOfType<GameManager>().EndGame();
        }
        
    }
}
